package org.lestvitsa.StudentsOrganizer.Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.util.Date;
import java.util.stream.Collectors;

import org.lestvitsa.StudentsOrganizer.TableImporter.Importable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;

public class Exercise implements Importable, Serializable {

	static MessageDigest digest;

	private static final long serialVersionUID = -5400341564741817442L;
	static {
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	private StringProperty creationDateProperty = new SimpleStringProperty();

	private StringProperty exerciseTextLatexProperty = new SimpleStringProperty();
	private StringProperty exerciseTextProperty = new SimpleStringProperty();

	private String hashId = new String();

	private StringProperty modificationDateProperty = new SimpleStringProperty();

	private StringProperty shortNameProperty = new SimpleStringProperty();

	private ObservableSet<ExerciseTag> subjectsProperty = FXCollections.observableSet();

	Exercise(String shortName, String exerciseText, Date creationDate) {
		shortNameProperty.setValue(shortName);
		exerciseTextProperty.setValue(exerciseText);
		
		creationDateProperty.setValue(DateFormat.getDateTimeInstance().format(creationDate));

		digest.reset();
		try {
			digest.update("#########################".getBytes("utf8"));
			digest.update(shortName.getBytes("utf8"));
			digest.update("#########################".getBytes("utf8"));
			digest.update(exerciseText.getBytes("utf8"));
			hashId = String.format("%040x", new BigInteger(1, digest.digest()));
			;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public StringProperty getCreationDateProperty() {
		return creationDateProperty;
	}
	
	public String getCreationDate() {
		return creationDateProperty.getValue();
	}

	public StringProperty getExerciseText() {
		return exerciseTextProperty;
	}

	public StringProperty getExerciseTextLatex() {
		return exerciseTextLatexProperty;
	}

	public String getHashId() {
		return hashId;
	}

	public StringProperty getModificationDateProperty() {
		return modificationDateProperty;
	}

	public StringProperty getShortNameProperty() {
		return shortNameProperty;
	}

	public ObservableSet<ExerciseTag> getSubjectsProperty() {
		return subjectsProperty;
	}

	public void setCreationDateProperty(StringProperty creationDateProperty) {
		this.creationDateProperty = creationDateProperty;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public void setModificationDateProperty(StringProperty modificationDateProperty) {
		this.modificationDateProperty = modificationDateProperty;
	}

	public void setSubjectsProperty(ObservableSet<ExerciseTag> subjectsProperty) {
		this.subjectsProperty = subjectsProperty;
	}

	public String toString() {
		return String.format("Exercise(%s,%s,%s)", shortNameProperty.getValue(), creationDateProperty.getValue(),
				getSubjectsProperty().stream().map(Object::toString).collect(Collectors.joining(", ")));
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		hashId = (String) s.readObject();
		shortNameProperty = new SimpleStringProperty((String) s.readObject());
		exerciseTextProperty = new SimpleStringProperty((String) s.readObject());
		exerciseTextLatexProperty = new SimpleStringProperty((String) s.readObject());

		creationDateProperty = new SimpleStringProperty((String) s.readObject());
		modificationDateProperty = new SimpleStringProperty((String) s.readObject());

		subjectsProperty = FXCollections.observableSet((ExerciseTag[]) s.readObject());
	}

	private void writeObject(ObjectOutputStream s) throws IOException {
		s.writeObject(hashId);
		s.writeObject(shortNameProperty.getValue());
		s.writeObject(exerciseTextProperty.getValue());
		s.writeObject(exerciseTextLatexProperty.getValue());

		s.writeObject(creationDateProperty.getValue());
		s.writeObject(modificationDateProperty.getValue());

		s.writeObject(getSubjectsProperty().toArray(new ExerciseTag[0]));
	}
}
