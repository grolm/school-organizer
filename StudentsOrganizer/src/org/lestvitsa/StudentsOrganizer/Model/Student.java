package org.lestvitsa.StudentsOrganizer.Model;

import java.io.Serializable;

import org.lestvitsa.StudentsOrganizer.TableImporter.Importable;

public class Student implements Importable, Serializable {

	private static final long serialVersionUID = 8215432560724158436L;

	private String firstName;
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String toString() {
		return String.format("Student(%s,%s)", firstName, lastName);
	}
}
