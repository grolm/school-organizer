package org.lestvitsa.StudentsOrganizer.TableImporter;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Importer {

	public static <T> ArrayList<T> load(Class<T> classToLoad, ArrayList<ArrayList<String>> list,
			String[] listOfFields) {

		ArrayList<T> result = new ArrayList<>();
		for (ArrayList<String> line : list) {
			T ri = null;
			try {
				ri = classToLoad.getDeclaredConstructor().newInstance();
			} catch (ReflectiveOperationException | SecurityException e) {
				e.printStackTrace();
				System.exit(-1);
			}

			int limit = listOfFields.length > line.size() ? line.size() : listOfFields.length;
			for (int i = 0; i < limit; i++) {
				String filedName = listOfFields[i];
				String value = line.get(i);
				try {
					Field declaredField = classToLoad.getDeclaredField(filedName);
					boolean accessible = declaredField.isAccessible();

					declaredField.setAccessible(true);

					Class<?> cls = declaredField.getType();
					if (cls == String.class)
					{
						declaredField.set(ri, value);
					}else if (cls.isAssignableFrom(SimpleStringProperty.class))
					{
						StringProperty pValue = new SimpleStringProperty();
						pValue.setValue(value);
						declaredField.set(ri, pValue);
					}
					declaredField.setAccessible(accessible);
				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {
					e.printStackTrace();
					// Failed to load
					ri = null;
				}

			}

			if (ri != null) {
				result.add(ri);
			}

		}

		return result;
	}

}
