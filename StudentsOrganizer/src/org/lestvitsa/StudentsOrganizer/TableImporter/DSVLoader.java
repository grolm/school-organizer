package org.lestvitsa.StudentsOrganizer.TableImporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DSVLoader {

	static abstract class FieldsCollector {
		ArrayList<ArrayList<String>> result = new ArrayList<>();

		abstract void collect(String aLine);

		public ArrayList<ArrayList<String>> getResult() {
			return result;
		}
	}

	static public class RegexpCollector extends FieldsCollector {
		private String regexp;

		public RegexpCollector(String aRegexp) {
			regexp = aRegexp;
		}

		@Override
		public void collect(String aLine) {
			String[] attributes = aLine.split(regexp);

			ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(attributes));

			result.add(stringList);
		}
	}

	public static class MultiLineCollector extends FieldsCollector {
		private String separator;
		private String currentBlock = new String();

		public MultiLineCollector(String sep) {
			separator = sep;
		}

		@Override
		public void collect(String aLine) {

			if (aLine.startsWith(separator)) {
				parseCurrentBlock();
				currentBlock = aLine;
			} else {
				if (!currentBlock.isEmpty()) {
					currentBlock += "\n";
				}
				currentBlock += aLine;
			}
		}

		private void parseCurrentBlock() {
			if (!currentBlock.isEmpty()) {
				String[] attributes = currentBlock.split(separator);

				ArrayList<String> stringList = new ArrayList<String>(
						Arrays.asList(Arrays.copyOfRange(attributes, 1, attributes.length)));

				result.add(stringList);

				currentBlock = new String();
			}
		}

		public ArrayList<ArrayList<String>> getResult() {
			parseCurrentBlock();
			return super.getResult();
		}

	}

	public static ArrayList<ArrayList<String>> load(File f, FieldsCollector collector) {
		Scanner scan = null;

		try {

			FileInputStream br = new FileInputStream(f);
			InputStreamReader isr = new InputStreamReader(br, Charset.forName("UTF-8"));
			BufferedReader bsr = new BufferedReader(isr);

			scan = new Scanner(bsr);

			while (scan.hasNextLine()) {
				String aLine = scan.nextLine();

				collector.collect(aLine);
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (scan != null) {
				scan.close();
			}
		}

		return collector.getResult();
	}

}
