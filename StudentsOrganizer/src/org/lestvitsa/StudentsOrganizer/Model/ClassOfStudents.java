package org.lestvitsa.StudentsOrganizer.Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.lestvitsa.StudentsOrganizer.TableImporter.Importable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ClassOfStudents implements Importable, Serializable {

	private static final long serialVersionUID = -5151584005938481718L;

	StringProperty classNameProperty = new SimpleStringProperty();

	ObservableList<Student> students = FXCollections.observableArrayList();

	public StringProperty classNameProperty() {
		return classNameProperty;
	}

	public ObservableList<Student> getStudents() {
		return students;
	}

	public ClassOfStudents(ArrayList<Student> students) {
		this.classNameProperty.set("<NO NAME>");
		this.students.addAll(students);
	}

	private void writeObject(ObjectOutputStream s) throws IOException {
		s.writeObject(classNameProperty.getValue());
		s.writeObject(getStudents().toArray(new Student[0]));
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		classNameProperty = new SimpleStringProperty((String) s.readObject());
		students = FXCollections.observableArrayList((Student[]) s.readObject());
	}
}
