package org.lestvitsa.StudentsOrganizer.Encoder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.lang.reflect.InvocationTargetException;

public class PlainEncoder implements Encoder {

	@Override
	public void encode(Serializable obj, String filepath) {
		try {
			FileOutputStream fos = new FileOutputStream(filepath);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Serializable decode(String filepath) {
		try {
			FileInputStream fis = new FileInputStream(filepath);
			ObjectInputStream ois = new ObjectInputStream(fis);
			return (Serializable) ois.readObject();
		} catch (ClassCastException | IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

}
