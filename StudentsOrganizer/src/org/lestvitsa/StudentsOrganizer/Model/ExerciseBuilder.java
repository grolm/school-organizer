package org.lestvitsa.StudentsOrganizer.Model;

import java.util.Date;

public class ExerciseBuilder {
	
	private String name;
	private String text;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	} 
	
	Exercise createExercise(Date date) {
		return new Exercise(name, text, date);
	}

}
