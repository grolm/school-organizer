package org.lestvitsa.StudentsOrganizer.UI;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.lestvitsa.StudentsOrganizer.Model.ClassOfStudents;
import org.lestvitsa.StudentsOrganizer.Model.Exercise;
import org.lestvitsa.StudentsOrganizer.Model.Organizer;
import org.lestvitsa.StudentsOrganizer.UI.Views.ClassView;
import org.lestvitsa.StudentsOrganizer.UI.Views.ExerciseView;
import org.lestvitsa.StudentsOrganizer.UI.Views.OrganizeClassesView;

import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class OrganizerWindowControllerFX extends Application {

	Organizer organizer;

	@Override
	public void start(Stage stage) {

		// String javaVersion = System.getProperty("java.version");
		// String javafxVersion = System.getProperty("javafx.version");
		// Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " +
		// javaVersion + ".");

		BorderPane mainPane = new BorderPane();
		MenuBar menuBar = new MenuBar();
		mainPane.setTop(menuBar);

		organizer = Organizer.loadCurrent(true);
		if (organizer == null) {
			organizer = new Organizer();
		}

		Menu menuClasses = createClassesMenu(stage, mainPane);

		Menu menuExercise = createExercisesMenu(stage, mainPane);
		Menu menuSprints = new Menu("Спринты");

		menuBar.getMenus().add(menuClasses);
		menuBar.getMenus().add(menuExercise);
		menuBar.getMenus().add(menuSprints);

		Scene scene = new Scene(mainPane, 1200, 800);
		stage.setScene(scene);
		stage.show();
	}

	private Menu createClassesMenu(Stage stage, BorderPane mainPane) {
		Menu menuClasses = new Menu("Классы");

		MenuItem menuClassesEdit = new MenuItem("Редактировать");
		menuClassesEdit.setOnAction(e -> {
			mainPane.setCenter(new OrganizeClassesView(organizer.getClasses()));
		});

		MenuItem menuClassesLoad = new MenuItem("Загрузить CSV");

		menuClassesLoad.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Выберите CSV файл со списком учеников");

			File file = fileChooser.showOpenDialog(stage);
			if (file != null) {
				organizer.loadClassFromCSV(file);
			}
		});

		MenuItem menuClassesSep = new SeparatorMenuItem();
		menuClasses.setOnShowing(e -> {
			menuClasses.getItems().remove(0, menuClasses.getItems().size() - 3);
			for (ClassOfStudents c : organizer.getClasses()) {
				MenuItem mi = new MenuItem(c.classNameProperty().get());
				mi.setOnAction(e1 -> {
					mainPane.setCenter(new ClassView(c, false));
				});
				menuClasses.getItems().add(menuClasses.getItems().size() - 3, mi);
			}
		});
		menuClasses.getItems().addAll(menuClassesSep, menuClassesLoad, menuClassesEdit);
		return menuClasses;
	}

	private Menu createExercisesMenu(Stage stage, BorderPane mainPane) {
		Menu menuClasses = new Menu("Задания");

		MenuItem menuClassesEdit = new MenuItem("Редактировать");
		menuClassesEdit.setOnAction(e -> {
			mainPane.setCenter(new OrganizeClassesView(organizer.getClasses()));
		});

		MenuItem menuClassesLoad = new MenuItem("Загрузить задания");

		menuClassesLoad.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Выберите файл со списком заданий");

			File file = fileChooser.showOpenDialog(stage);
			if (file != null) {
				organizer.loadExercisesFromCSV(file);
			}
		});

		MenuItem menuClassesSep = new SeparatorMenuItem();
		menuClasses.setOnShowing(e -> {

			menuClasses.getItems().remove(0, menuClasses.getItems().size() - 3);

			Map<String, List<Exercise>> byDate = organizer.getExercises().entrySet().stream().map(en -> en.getValue())
					.collect(Collectors.groupingBy(Exercise::getCreationDate));

			for (Map.Entry<String, List<Exercise>> pair : byDate.entrySet()) {

				Menu mi = new Menu(pair.getKey());

				List<Exercise> sortedList = pair.getValue().stream().sorted((e1, e2) -> e1.getShortNameProperty()
						.getValue().compareTo(e2.getShortNameProperty().getValue())).collect(Collectors.toList());

				for (Exercise ex : sortedList) {
					MenuItem smi = new MenuItem(ex.getShortNameProperty().get());
					mi.getItems().add(smi);
					smi.setOnAction(e1 -> {
						mainPane.setCenter(new ExerciseView(ex));
					});
				}
				menuClasses.getItems().add(menuClasses.getItems().size() - 3, mi);
			}
		});
		menuClasses.getItems().addAll(menuClassesSep, menuClassesLoad, menuClassesEdit);
		return menuClasses;
	}

	@Override
	public void stop() {
		System.out.println("Stage is closing");
		if (organizer != null) {
			organizer.saveCurrent();
		}
	}

	public static void main(String[] args) {
		launch();
	}

}