package org.lestvitsa.StudentsOrganizer.UI.Views;

import org.lestvitsa.StudentsOrganizer.Model.ClassOfStudents;
import org.lestvitsa.StudentsOrganizer.Model.Student;

import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

public class ClassView extends BorderPane {

	public ClassView(ClassOfStudents classObj, boolean organizeClassMode) {

		TableView<Student> table = new TableView<Student>();
		table.setItems(classObj.getStudents());

		TableColumn<Student, String> lnColumn = new TableColumn<Student, String>("Фамилия");
		lnColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("lastName"));
		lnColumn.setMinWidth(150);
		table.getColumns().add(lnColumn);

		TableColumn<Student, String> fnColumn = new TableColumn<Student, String>("Имя");
		fnColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("firstName"));
		fnColumn.setMinWidth(150);
		table.getColumns().add(fnColumn);

		setCenter(table);

		if (organizeClassMode) {
			table.setDisable(true);

			TextField topText = new TextField();
			Bindings.bindBidirectional(topText.textProperty(), classObj.classNameProperty());
			topText.setAlignment(Pos.CENTER);
			topText.setEditable(true);
			topText.setFocusTraversable(false);

			setTop(topText);
		} else {
			Label topLabel = new Label();
			topLabel.textProperty().bind(classObj.classNameProperty());
			topLabel.setAlignment(Pos.CENTER);
			topLabel.setFocusTraversable(false);
			topLabel.setStyle("-fx-font: 24 arial;");
			widthProperty().addListener((obs, oldVal, newVal) -> topLabel.setMinWidth(newVal.doubleValue()));

			setTop(topLabel);
		}
	}

}
