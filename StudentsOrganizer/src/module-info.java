/**
 * @author grolm
 *
 */
module StudentsOrganizerJFX {
	requires javafx.graphics;
	requires javafx.controls;
	requires java.desktop;
	requires java.logging;
	requires javafx.base;
	requires java.base;

	exports org.lestvitsa.StudentsOrganizer.UI;

	opens org.lestvitsa.StudentsOrganizer.Model to javafx.base;

}