package org.lestvitsa.StudentsOrganizer.UI.Views;

import org.lestvitsa.StudentsOrganizer.Model.Exercise;

import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class ExerciseView extends BorderPane {

	private Exercise execise;

	public ExerciseView(Exercise execise) {

		this.execise = execise;

		TextField topText = new TextField();
		Bindings.bindBidirectional(topText.textProperty(), execise.getShortNameProperty());
		topText.setAlignment(Pos.CENTER);
		topText.setEditable(true);
		topText.setFocusTraversable(false);

		TextArea centerText = new TextArea();
		Bindings.bindBidirectional(centerText.textProperty(), execise.getExerciseText());
		centerText.setEditable(true);
		centerText.setFocusTraversable(false);
		
		setCenter(centerText);
		
		setTop(topText);		
	}
}
