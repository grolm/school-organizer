package org.lestvitsa.StudentsOrganizer.Encoder;

import java.io.Serializable;

public interface Encoder {
	void encode(Serializable obj, String filepath);

	Serializable decode(String filepath);
}
