package org.lestvitsa.StudentsOrganizer.UI.Views;

import org.lestvitsa.StudentsOrganizer.Model.ClassOfStudents;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class OrganizeClassesView extends BorderPane {

	public OrganizeClassesView(ObservableList<ClassOfStudents> classes) {
		TableView<ClassOfStudents> table = new TableView<ClassOfStudents>();
		table.setItems(classes);

		TableColumn<ClassOfStudents, String> classColumn = new TableColumn<ClassOfStudents, String>("Класс");
		classColumn.setCellValueFactory(new PropertyValueFactory<ClassOfStudents, String>("className"));
		classColumn.setMinWidth(200);
		table.getColumns().add(classColumn);

//		TableColumn<Student, String> fnColumn = new TableColumn<Student, String>("Имя");
//		fnColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("firstName"));
//		fnColumn.setMinWidth(150);
//		table.getColumns().add(fnColumn);		

		setLeft(table);

		BorderPane classPane = new BorderPane();

		HBox topClassPane = new HBox();
		classPane.setTop(topClassPane);

		HBox bottomClassPane = new HBox();
		classPane.setBottom(bottomClassPane);

		setCenter(classPane);

		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				classPane.setCenter(new ClassView(newSelection, true));
			}
		});
	}

}
