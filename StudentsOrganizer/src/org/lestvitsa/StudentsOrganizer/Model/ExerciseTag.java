package org.lestvitsa.StudentsOrganizer.Model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.lestvitsa.StudentsOrganizer.TableImporter.Importable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ExerciseTag implements Importable, Serializable {

	private static final long serialVersionUID = -2060700287045880033L;
	
	private StringProperty nameProperty = new SimpleStringProperty();

	public StringProperty getNameProperty() {
		return nameProperty;
	}
	
	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		nameProperty = new SimpleStringProperty((String) s.readObject());;
	}

	private void writeObject(ObjectOutputStream s) throws IOException {
		s.writeObject(nameProperty.getValue());
	}

}
