package org.lestvitsa.StudentsOrganizer.Model;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.lestvitsa.StudentsOrganizer.Encoder.PlainEncoder;
import org.lestvitsa.StudentsOrganizer.TableImporter.DSVLoader;
import org.lestvitsa.StudentsOrganizer.TableImporter.Importer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

public class Organizer implements Serializable {

	static Logger logger = Logger.getLogger(Organizer.class.getName());

	private static final long serialVersionUID = -9141490568519055544L;

	private static File getFileByDate(Date d) {
		File dir = new File(".");
		String date = new SimpleDateFormat("dd-MM-yyyy").format(d);
		return new File(dir, "School " + date + ".org");
	}

	public static Organizer load(File f) {

		logger.log(Level.INFO, "Load from " + f.getAbsolutePath());

		Organizer result = null;

		if (f.exists()) {
			result = (Organizer) new PlainEncoder().decode(f.getAbsolutePath());
		}
		if (result == null) {
			result = new Organizer();
			result.save(f);
		}

		return result;
	}

	public static Organizer loadCurrent(boolean loadPrevious) {
		Date date = new Date();
		File currentFile = null;
		final long oneDay = 1000L * 24 * 3600;
		Date stopDate = new Date(date.getTime() - oneDay * 1000);
		do {
			currentFile = getFileByDate(date);
			date.setTime(date.getTime() - oneDay);
		} while (loadPrevious && stopDate.before(date) && (currentFile == null || !currentFile.exists()));

		if (currentFile != null) {
			return load(currentFile);
		} else {
			return null;
		}
	}

	ObservableList<ClassOfStudents> classes = FXCollections.observableArrayList();

	ObservableMap<String, Exercise> exercises = FXCollections.observableMap(new HashMap<String, Exercise>());

	ObservableList<Student> students = FXCollections.observableArrayList();

	{
		logger.addHandler(new ConsoleHandler());
	}

	public ObservableList<ClassOfStudents> getClasses() {
		return classes;
	}

	public ObservableMap<String, Exercise> getExercises() {
		return exercises;
	}

	public ObservableList<Student> getStudents() {
		return students;
	}

	public void loadClassFromCSV(File file) {
		ArrayList<ArrayList<String>> listOfLines = DSVLoader.load(file, new DSVLoader.RegexpCollector("\\s+"));
		ArrayList<Student> students = Importer.load(Student.class, listOfLines,
				new String[] { "firstName", "lastName" });

		classes.add(new ClassOfStudents(students));
	}

	public void loadExercisesFromCSV(File file) {
		ArrayList<ArrayList<String>> listOfLines = DSVLoader.load(file, new DSVLoader.MultiLineCollector("#"));
		ArrayList<ExerciseBuilder> eBuilders = Importer.load(ExerciseBuilder.class, listOfLines,
				new String[] { "name", "text" });

		Date d = new Date();
		
		List<Exercise> exercises = eBuilders.stream().map(builder -> builder.createExercise(d))
				.collect(Collectors.toList());

		addNewExercises(exercises);
	}

	private void addNewExercises(List<Exercise> exercises) {
		ObservableMap<String, Exercise> exs = getExercises();
		for (Exercise ex : exercises) {
			if (!exs.containsKey(ex.getHashId())) {
				exs.put(ex.getHashId(), ex);
			}
		}
	}

	public void save(File f) {
		logger.log(Level.INFO, "Save to " + f.getAbsolutePath());
		new PlainEncoder().encode(this, f.getAbsolutePath());
	}

	public void saveCurrent() {
		save(getFileByDate(new Date()));
	}

	private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
		classes = FXCollections.observableArrayList((ClassOfStudents[]) s.readObject());
		students = FXCollections.observableArrayList((Student[]) s.readObject());
		exercises = FXCollections.observableMap((Map<String, Exercise>) s.readObject());
	}

	private void writeObject(ObjectOutputStream s) throws IOException {
		s.writeObject(getClasses().toArray(new ClassOfStudents[0]));
		s.writeObject(getStudents().toArray(new Student[0]));
		s.writeObject(new HashMap<>(getExercises()));
	}
}
